/* global jQuery, __page */

'use strict';

jQuery(document).ready(function($) {
  // Make nav link active using the global __page var
  const selectedNavPoint = $('.link-' + __page);

  if (selectedNavPoint) {
    selectedNavPoint.addClass('active');
    const pointLinks = selectedNavPoint.children('.nav-link');
    // Add screen reader sign to the selected menu point
    // this condition we need to prevent whole script crash
    // in case of the point doesn't have such an element
    if (pointLinks.length > 0) {
      $('<span/>').addClass('sr-only').text('(current)').appendTo(pointLinks[0]);
    }
  }

});
