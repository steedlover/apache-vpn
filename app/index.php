<?php
$page = $_GET["page"];
$page = $page ? $page : "main";
?>

<!doctype html>
<html lang="en">
    <head>
        <title>Document</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="description" content="Test application">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        <meta name="author" content="Dmitry Moskin">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320"/>

        <link rel="stylesheet" type="text/css" href="./css/styles.css">

        <script src="./js/base.js" defer></script>
        <script src="./js/scripts.js" defer></script>
        <script>
            const __page = '<?=$page?>';
        </script>
        <!--[if lt IE 9]><script type="text/javascript" src="./js/html5shiv.js"></script><![endif]-->
    </head>
    <body>
        <header>
            <!-- The navigation part could be inserted here instead of separate file.
            First I wanted remove all repeating elements to separate files.
            But I figured out soon I can remove the content pages instead -->
            <?php require_once("./partials/nav.php") ?>
        </header>
        <main>
            <?php require_once("./pages/" . $page . ".php"); ?>
        </main>
        <!-- Footer -->
        <footer><div class="container">Target Traffic LTD</div></footer>
    </body>
</html>
