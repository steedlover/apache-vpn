'use strict';

// include the required packages.
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const stripCssComments = require('gulp-strip-css-comments');

const buildPath = 'build/';

/* Static js files */
gulp.task('staticjs', function () {
  gulp.src('app/static/js/*.js')
    .pipe(gulp.dest(buildPath + 'js'));
});

gulp.task('js', function() {
  gulp.src('app/js/**/*.js')
    .pipe($.babel({
      presets: ['es2015', 'stage-2']
    }))
    .pipe($.concat('scripts.js'))
    .pipe(gulp.dest(buildPath + '/js'));
});

/* minify images */
gulp.task('images', function () {
  gulp.src('app/img/**/*')
    .pipe($.imagemin({
      progressive: true,
      interlaced: true
    }))
    // Change mod here is very important
    // otherwise images are not available for apache
    // inside the  docker container
    .pipe($.chmod(0o777))
    .pipe(gulp.dest(buildPath + 'img'));
});

/* minify pages */
gulp.task('minify-pages', function () {
  gulp.src('app/pages/*.php')
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeTagWhitespace: true,
      minifyJS: true
    }))
    .pipe(gulp.dest(buildPath + 'pages'));
});

/* minify partials */
gulp.task('minify-partials', function () {
  gulp.src('app/partials/*.php')
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeTagWhitespace: true,
      minifyJS: true
    }))
    .pipe(gulp.dest(buildPath + 'partials'));
});

/* minify index */
gulp.task('minify-index', function () {
  gulp.src('app/index.php')
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeTagWhitespace: true,
      minifyJS: true
    }))
    .pipe(gulp.dest(buildPath));
});

gulp.task('basejs', function() {
  gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.js'
  ])
    .pipe($.uglifyjs())
    .pipe($.concat('base.js'))
    .pipe(gulp.dest(buildPath + '/js'));
});

// Get one .styl file and render
gulp.task('styles', function () {
  return gulp.src('app/styles/styles.styl')
    .pipe($.stylus({
      compress: true,
      'include css': true,
    }))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe(stripCssComments({
      preserve: false
    }))
    .pipe($.csso())
    .pipe(gulp.dest(buildPath + '/css'));
});

// Default gulp task to run
gulp.task('default', ['minify-partials', 'minify-index', 'minify-pages', 'js', 'basejs', 'staticjs', 'styles', 'images', 'watch']);
gulp.task('build', ['minify-partials', 'minify-index', 'minify-pages', 'js', 'basejs', 'staticjs', 'styles', 'images']);
gulp.task('watch', function () {
  $.watch('app/index.php', () => gulp.start('minify-index'));
  $.watch('app/partials/*.php', () => gulp.start('minify-partials'));
  $.watch('app/pages/*.php', () => gulp.start('minify-pages'));
  $.watch('app/styles/**/*.styl', () => gulp.start('styles'));
  $.watch('app/js/**/*.js', () => gulp.start('js'));
});


