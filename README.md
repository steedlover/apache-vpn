>##Stylus
>
>On reading the role description I found a requirement to be expirienced working with SASS css preprocessor. Since currently I'm working with Stylus and both of them are quite similar I decided to use it here. For using Stylus I needed something to assemble CSS and I chose GULP. With GULP I connected a lot of cool features such as HTML minifying, JS uglifying, images optimizing etc.

>##Red rectangle
>
>There is a string in the task description- "On this website place a red rectangle in the middle of the browser-tab". I didn't quite understand what "browser-tab" means here and placed the red rectangle in the center of the page.
>On the desktop devices the rectangle is placed in the center of page and on tablets/phones on the top of the page. Sticky footer on this page for desktops only.

>##Slider
>
>The most simpliest way to implement slider was to take any jQuery plugin. But I decided to do it from scratch. Since this is a test of my skills.
>Main concept was taken from W3 site (https://www.w3schools.com/w3css/w3css_slideshow.asp). I assembled my slider from several samples and created styles for STYLUS.
>I added an extra tag for main tag in the slider page and set more height for it. The requirement is fulfilled and on mobile resolution footer is floated I just set a bit bigger height for best view on thsese devices.

>##Docker usage
>
>There is a line in the task description what says: "You don’t have to use the given docker image if you decide to not use apache/php, tho you are strongly encouraged to use docker"
>The "tho" word looks like misspelling at the end of sentence. I focused on a phrase "are strongly encouraged to use docker" and chose your platform.
>
>You offerd to use PHP here  but if I had a task without any instructions regarding using certain platform I would select AngularJS. In accordance with the task I have to prepare several content pages. AngularJS has route module for these purposes and it allows make pages separatly of main HTML scaffold. Here I did several PHP pages which are being collected via require function. Since I'm using PHP I have to recognize the current page, I created the javascript global variable __page for this. It doesn't look so gracefully as AngularJS approach but it works and allows to create a lot of separate pages without changing the main HTML scaffold. In the readme file I found a string about future scenario of the site extending. That's my solution.
>
>I faced the problem of the files permissions on mounting my folder to the docker container. Apache couldn't read the images. I added a special directive to the gulp file for giving full access to images folder on the application building. I left a comment in the gulp file.

>##Comments
>
>Also I left some comments in the code with my thoughts descriptions regarding different parts of code.

>##Time
>
>I did initialize the git repository and have been committing my changes out quite often. So you can check how much time it did take for me doing this test.


##How to start the project##

###1) Cloning the repo###
```
#!bash
git clone https://steedlover@bitbucket.org/steedlover/apache-vpn.git ./
```

###2) Installing dependencies###
```
#!bash
npm i
```
###3) Building the project###
```
#!bash
gulp build
```
###4) Starting on the docker container###
```
#!bash
docker run -d --name=apache-php --restart=always -p 8080:80 -v "$PWD/build":/var/www/html chriswayg/apache-php
```
>After that project will be available by the http://localhost:8080 address
>I prefer to start container on port 8080 cause port 80 is not available for me (it's usually taken already by apache/nginx/lighttpd).
