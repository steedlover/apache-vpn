/* global jQuery */

'use strict';

jQuery(document).ready(function($) {

  let slideIndex = 1;
  let sliderContainerClass = '.slides-container';
  let cntrlPaddleClass = '.bottom-paddle';
  let badgeClassName = 'badge-counter';
  let badgeActiveClassName = 'active';
  let cntrlButtonClass = '.slider-button';
  let leftBtnClassName = 'btn-left';
  let rightBtnClassName = 'btn-right';
  let slideClass = '.img-slide';
  let visibleClassName = 'visible';

  let slidePage = false;
  if ($(sliderContainerClass).length) {
    slidePage = true;
  }

  // Count images and set control badges
  $(slideClass).each((n) => {
    n += 1;
    let badge = $('<div/>').attr('id', 'b' + n).addClass(badgeClassName);
    $(cntrlPaddleClass).append(badge);
  });

  // start loop for the slider and set first slide
  // only if the current page has slider-container block
  if (slidePage) {
    // Add extra class to the main block on the slider page
    $('main').addClass('slider');
    showDivs(slideIndex);
    setInterval(() => showDivs(slideIndex + 1), 3000);
  }

  // Listener of the arrows clicking
  $(cntrlButtonClass).on('click', (e) => {
    let n = 0;
    let btn = e.currentTarget;
    if ($(btn).hasClass(leftBtnClassName)) {
      n = -1;
    } else if ($(btn).hasClass(rightBtnClassName)) {
      n = 1;
    }
    // It could be written like this
    // n = $(this).hasClass(leftBtnClassName) ? -1 : ($(this).hasClass(rightBtnClassName) ? 1 : 0);
    // but this code style is quite hard to read on further maintaining of the application, so I prefer to avoid such as line
    if (n !== 0) {
      showDivs(slideIndex += n);
    }
  });

  // Listener of the badges clicking
  $('.' + badgeClassName).on('click', (e) => {
    const el = $(e.currentTarget);
    const elId = el.attr('id');
    // If elId doesn't exist further string will crash the script
    if (elId) {
      // Since we have as a letter the first character only
      // we should cut the first letter and it will be the number as a string
      // so we should apply the parseInt method also
      const n = parseInt(elId.substring(1));
      // and change the slide using the selected badge number
      showDivs(n);
    }
  });

  function showDivs(n) {
    const imgsArr = $(slideClass);
    if (n > imgsArr.length) {
      slideIndex = 1;
    } else if (n < 1) {
      slideIndex = imgsArr.length;
    } else {
      slideIndex = n;
    }
    $(slideClass).removeClass(visibleClassName);
    $.each(imgsArr, (num, el) => {
      num += 1;
      if (num === slideIndex) {
        $(el).addClass(visibleClassName);
      }
    });
    // Remove active class of all badges first
    $('.' + badgeClassName).removeClass(badgeActiveClassName);
    // Set active badge class
    const currentBadgeEl = $('.' + badgeClassName)[slideIndex - 1];
    $(currentBadgeEl).addClass(badgeActiveClassName);
  }

});
