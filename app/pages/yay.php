<div class="slides">
    <div class="slides-container">

        <img class="img-slide" src="img/1.jpg" />
        <img class="img-slide" src="img/2.jpg" />
        <img class="img-slide" src="img/3.jpg" />
        <img class="img-slide" src="img/4.jpg" />
        <img class="img-slide" src="img/5.jpg" />

        <div class="bottom-paddle">
            <div class="slider-button btn-left">&#10094;</div>
            <div class="slider-button btn-right">&#10095;</div>
        </div>

    </div>
</div>
